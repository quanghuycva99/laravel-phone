<?php

use App\Http\Controllers\PhoneController;
use App\Http\Cotrollers;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'App\Http\Controllers\PhoneController@index')->name('phones');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::resource('profile', 'App\Http\Controllers\ProfileController');
Route::resource('phones', 'App\Http\Controllers\PhoneController');
Route::resource('categories', 'App\Http\Controllers\CategoryController');
Route::put('/phones/update/{id}', [PhoneController::class, 'update'])->name('phones.update');
Route::get('/phones/edit/{id}', [PhoneController::class, 'edit'])->name('phones.edit');

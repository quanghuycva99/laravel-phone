@extends('layouts.layout')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"> Edit user Information</div>

                    <div class="card-body">

                        @if(count($errors)>0)
                            <ul class="navbar-nav mr-auto">
                                @foreach ($errors->all() as $error)
                                    <li class="nav-item active">
                                        {{$error}}
                                    </li>
                                @endforeach

                            </ul>
                        @endif

                        <form action="{{route('profile.update', $user->id)}}" method="POST" enctype="multipart/form-data" >
                            {{ csrf_field()}}
                            {{ method_field('PATCH') }}

                            <input name="id" type="hidden" value="{{ $user->id }}">

                            <div class="form-group">
                                <label for="avatar">Update Profile Image</label>
                                <input type="file" class="form-control-file" name=avatar>
                            </div>

                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name"  value="{{$user->name}}">
                            </div>

                            <div class="form-group">
                                <label for="name">Username</label>
                                <input type="text" class="form-control" name="username"  value="{{$user->username}}">
                            </div>

                            <div class="form-group">
                                <label for="name">Email Address</label>
                                <input type="text" class="form-control" name="email"  value="{{$user->email}}">
                            </div>

                            <button type="submit" class="btn btn-primary">Update</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


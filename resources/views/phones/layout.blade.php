<!DOCTYPE html>
<html>
    <head>
        <title>Mobile Phones Store</title>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet"/>
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Great+Vibes&family=Open+Sans&family=Roboto&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Great+Vibes&family=Open+Sans&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">  
    <style>
         body {
             background-image: url("https://wallpapercave.com/wp/wp5529280.jpg");
             background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100% 100%;
         }
         table{
             font-family: Roboto;
             font-size: 18px;
         }
         
    </style>
    
    </head>
<body >
<div class="container">
    @yield('content')
</div>
</body>
</html>
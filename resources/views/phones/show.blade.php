@extends('layouts.layout')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3> <strong>Entry Detail</strong> </h3>
                <hr>
            </div>
        </div>
    </div><br>
    <div class="row justify-content-center">
    <div class="col-sm-5">
        <img
            src="{{ asset('/uploads/img/' . $phone->image_path) }}"
            style="width:70%;height:auto;"
        >
    </div>
    <div class="col-sm-5">
        <br>
        <br>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group" style="font-size:30px">
                <strong>Name:</strong>
                {{ $phone->name }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group" style="font-size:30px">
                <strong>Category:</strong>
                {{ $phone->category->name  }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group" style="font-size:30px">
                <strong>Quantity:</strong>
                {{ $phone->quantity }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group" style="font-size:30px">
                <strong>Price per unit:</strong>
                {{ $phone->price }}
                <hr>
                <a class="btn btn-primary" href="{{ route('phones.index') }}">Back</a>
            </div>

    </div>
    </div>
@endsection

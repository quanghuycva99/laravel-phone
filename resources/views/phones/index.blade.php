@extends('layouts.layout')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> <strong> Product Information </strong></h2>
                <hr>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <span>{{ $message }}</span>
    </div>
    @endif
    <table class="table " style="background-color: rgba(0,0,0,0.2); color:white;">
        <tr>
            <th>Name</th>
            <th>Category</th>
            <th>Quantity</th>
            <th>Price per unit</th>
            <th>Action</th>
        </tr>
        @foreach ($phones as $phone)
        <tr>
            <td>{{ $phone->name }}</td>
            <td>{{ $phone->category->name }}</td>
            <td>{{ $phone->quantity }}</td>
            <td>{{ $phone->price }}</td>
            <td><form action="{{ route('phones.destroy',$phone->id) }}" method="POST">
            <a class="btn btn-info" href="{{ route('phones.show',$phone->id) }}">Show</a>
            <a class="btn btn-warning" href="{{ route('phones.edit',$phone->id) }}">Edit</a>
            @csrf
            @method('DELETE')
            <button type="submit" onclick="return confirm('Do you really want to delete this entry?')" class="btn btn-danger">Delete</button>
            </form></td>
        </tr>
        @endforeach
    </table>
{!! $phones->links('pagination::bootstrap-4') !!}
@endsection

@extends('layouts.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2><strong>Manage Categories</strong></h2>
            </div>
        </div>
    </div>
    <hr>
        <table class="table " style="background-color: rgba(0,0,0,0.2); color:white;">
            <tr>
                <th><strong>Name</strong></th>
                <th><strong>Action</strong></th>
            </tr>
            @foreach($categories as $category)
                <tr>
                    <td>{{ $category->name }}</td>
                    <td>
                        <form action="{{ route('categories.destroy', $category->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a class="btn btn-primary" href="{{ route('categories.edit', $category->id) }}">Edit</a>
                            <button class="btn btn-danger" onclick="return confirm('Do you really want to delete this entry?')">Delete</button>
                            <a class="btn btn-info" href="{{ route('categories.show', $category->id) }}">Show</a>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
        {{ $categories->links('pagination::bootstrap-4') }}
@endsection

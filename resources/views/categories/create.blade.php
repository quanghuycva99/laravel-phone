@extends('layouts.layout')

@section('content')
    <div class="container">
        <h1>Add a new category</h1>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{ route('categories.store') }}" method="POST" >
            @csrf
            <span>Name</span>
            <input type="text" class="form-control" name="name" placeholder="Enter name ...">
            <br>
            <button type="submit" class="btn btn-success mt-1">Submit</button>
            <br>
            <br>
            <a class="btn btn-primary" href="{{ route('categories.index') }}">Show all categories</a>
        </form>
    </div>
@endsection

@extends('layouts.layout')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3> <strong>Category Detail</strong> </h3>
                <hr>
            </div>
        </div>
    </div><br>
    <div class="card-deck">
        @foreach($phones as $phone)
        @if($phone->category_id == $id)
            <div class="card text-white" style="max-width: 30rem; background-color: rgba(0,0,0,0.2);">
                <img class="card-img-top wrapper1" style="text-align:center;"
                        src="{{ asset('/uploads/img/' . $phone->image_path) }}">
                <div class="card-body">
                <h5 class="card-title">
                            <strong>Name:</strong>
                            {{ $phone->name }}
                </h5>
                <p class="card-text">
                    Quantity: {{ $phone->quantity }}
                <br>
                    Price per unit: {{ $phone->price }}
                <br>
                </p>
                    <a class="btn btn-success mb-1" href="{{url('/phones/edit/'.$phone->id)}}"> Edit </a>
                </div>
            </div>
        @endif
         @endforeach
    </div>
    <br>
    <a class="btn btn-primary" href="{{ url()->previous() }}">Back</a>
@endsection

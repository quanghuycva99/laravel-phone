@extends('layouts.signinlayout')
@section('content')
                    <form class="form-signin" method="POST" action="{{ route('login') }}">
                        <img class="mb-4" src="{{url('/uploads/backgrounds/logo1.png')}}" alt="" width="50%" height="50%">
                        @csrf
                        <h1 class="h3 mb-3 font-weight-normal"><strong>Welcome!</strong> <hr></h1>
                            <label for="email" class="sr-only">{{ __('E-Mail/Username') }}</label>
                            <input id="email" type="text" placeholder="Email address/Username" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            <label for="password" class="sr-only">{{ __('Password') }}</label>
                            <input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        <div class="checkbox mb-3">
                            <br>
                            <label>
                                <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> Remember me
                            </label>
                        </div>
                                <button type="submit" class="btn btn-lg btn-primary">
                                    {{ __('Login') }}
                                </button>
                                <a class="btn btn-lg btn-success" href="{{ route('register') }}">
                                    {{ __('Register') }}
                                </a>
                            @if (Route::has('password.request'))

                            <br>
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                    </form>
@endsection

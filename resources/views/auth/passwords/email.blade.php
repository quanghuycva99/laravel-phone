@extends('layouts.registerlayout')

@section('content')

                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-signin" method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <h1 class="text-center">
                            <img class="mb-4" src="{{url('/uploads/backgrounds/logo1.png')}}" alt="" width="50%" height="50%">
                            <br>
                            <strong>Forgot Your Password?</strong>
                            <hr>
                        </h1>

                            <label for="email"> E-mail address</label>
                            <input id="email" placeholder="Please enter your e-mail address..." type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback text-center" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        <br>
                <div class="text-center">
                    <button type="submit" class="btn btn-primary">
                             {{ __('Send Password Reset Link') }}
                    </button>
                    <br>
                    <br>
                        <a class="btn btn-secondary" href="{{ route('login') }}"> Back </a>

                </div>

                    </form>
@endsection

@extends('layouts.registerlayout')

@section('content')
                    <form class="form-signin" method="POST" action="{{ route('register') }}">
                        @csrf
                        <h1 class="text-center">
                            <img class="mb-4" src="{{url('/uploads/backgrounds/logo1.png')}}" alt="" width="50%" height="50%">
                            <br>
                            <strong>Register!</strong>
                            <hr>
                        </h1>

                                <label for="name" >Full Name</label>
                                <input id="name" type="text" placeholder= "Please enter your full name..." class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            <div {{ $errors->has('username') ? ' has-error' : '' }}">
                                <br>
                                <label for="username" >Username</label>
                                <input id="username" type="text" class="form-control" placeholder= "Please enter your username..." name="username" value="{{ old('username') }}" required>
                                     @if ($errors->has('username'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('username') }}</strong>
                                        </span>
                                    @endif
                             </form>

                                <br>
                                <label for="email" >E-mail Address</label>
                                <input id="email" type="email" placeholder="Please enter your email address..." class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                            <br>
                            <label for="password" >Password</label>
                            <input id="password" type="password" placeholder="Please enter your password..." class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            <br>
                            <label for="password-confirm" >Confirm Password</label>
                            <input id="password-confirm" type="password" class="form-control" placeholder = "Please re-enter your password..." name="password_confirmation" required autocomplete="new-password">
                        <div class="text-center">
                            <br>
                            <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                            </button>
                            <br>
                            <br>
                                <a class="btn btn-secondary" href="{{ route('login') }}">
                                    Back
                                </a>
                    </div>
                    </form>
@endsection

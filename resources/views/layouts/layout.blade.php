<!doctype html>
<html lang="en">
<head>
    <title>TNX Product Management</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <script src="https://kit.fontawesome.com/5879cda407.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" >
</head>
<body>
<style>
    div.c {
        font-size: 150%;
</style>
<div class="wrapper d-flex align-items-stretch">
    <nav id="sidebar" class="img" style="background-image: url(/uploads/backgrounds/login2.jpg);">
        <div class="p-4 pt-5">
            <a href="{{ route('profile.index') }}">
                <img class="img logo rounded-circle mb-5" src="/uploads/avatars/{{ Auth::user()->avatar }}">
                <div class="text-center text-uppercase c">
                    <strong>{{ Auth::user()->name }}</strong>
                </div>
            </a>
            <br>
            <ul class="list-unstyled components mb-5">
                <li>
                    <a href={{ route('phones.index') }}>
                            <span
                                class="fa fa-home mr-3">
                            </span>
                                Home
                    </a>
                </li>

                <li>
                    <a href={{ route('phones.create') }}>
                            <span
                                class="fas fa-pencil-alt mr-3">
                            </span>
                        Create A New Entry</a>
                </li>

                <li>
                    <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span
                            class="fas fa-archive mr-3">
                            </span>
                        Categories</a>
                    <ul class="collapse list-unstyled" id="homeSubmenu">
                        <li>
                            <a href={{ route('categories.index') }}>Manage Categories</a>
                        </li>
                        <li>
                            <a href="{{ route('categories.create') }}">Create a new Category</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="{{ route('logout') }}">
                        <span
                            class="fas fa-sign-out-alt mr-3">
                        </span>
                        Logout</a>
                </li>
            </ul>
            <div class="footer text-center">
                <p> Made by Deha with love!!!
            </div>

        </div>
    </nav>
    <style>
        body{
            background-image: url('http://t0.gstatic.com/licensed-image?q=tbn:ANd9GcT-JpGcO97xjNnFW-X_sGqM2nHz6L0PF8Shd-8P9u3mlAEFdM8y2JJ1TPiWZ-PWv9JhroVs4NBo9EZvirVPbSk');
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100% 100%;
            color: black;
        }
    </style>
<!-- Page Content  -->
<div id="content" class="p-4 p-md-5">
@yield('content')
<script type="text/javascript" src="{{ URL::asset('js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/popper.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/main.js') }}"></script>

</div>
</div>
</body>
</html>


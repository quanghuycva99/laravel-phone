<!DOCTYPE html>
<html>
<head>
    <title>TNX Product Management</title>
    <!-- Bootstrap core CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet"/>
    <!-- Custom styles for this template -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/signin.css') }}" >
</head>

<body class="text-center">
<style>
    body{
        background-image: url('/uploads/backgrounds/login2.jpg');
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: 100% 100%;
        color: white;
    }
</style>
@yield('content')
</body>
</html>

<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePhoneRequest;
use App\Http\Requests\UpdatePhoneRequest;
use App\Http\Requests\UpdateProfileRequest;
use App\Models\Phone;
use Illuminate\Http\Request;
Use App\Models\Category;

class PhoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $phones = Phone::with('category')->paginate(10);
        return view('phones.index', compact('phones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::get();
        return view('phones.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreatePhoneRequest $request)
    {
        if($request->hasFile('image_path')) {
            $image_path = request()->file('image_path');
            $extension = $image_path->getClientOriginalExtension();
            $newImageName = time() . '-' . $request->name . '.' . $extension;
            $request->image_path->move(public_path('/uploads/img/'), $newImageName);
        }
        $phone = Phone::create([
            'name' => $request->input('name'),
            'quantity' => $request->input('quantity'),
            'price' => $request->input('price'),
            'image_path' => $newImageName,
            'category_id' => $request->input('category_id')
        ]);

        return redirect()->route('phones.index')->with('success','Phone created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Phone  $phone
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function show(Phone $phone)
    {
        return view('phones.show',compact('phone'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Phone  $phone
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit($id)
    {
        $phone=Phone::findOrFail($id);
        $categories=Category::get();
        return view('phones.edit',compact('phone','categories'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdatePhoneRequest $request, $id): \Illuminate\Http\RedirectResponse
    {
        $phone=Phone::findOrFail($id);

        $phone->id = $request->input('id');
        $phone->name = $request->input('name');
        $phone->quantity = $request->input('quantity');
        $phone->price = $request->input('price');
        $phone->category_id = $request->input('category_id');

        if($request->hasFile('image_path')) {
            $image_path = request()->file('image_path');
            $extension = $image_path->getClientOriginalExtension();
            $filename = time() . '-' . $request->name . '.' . $extension;
            $image_path->move(public_path('/uploads/img/'), $filename);
            $phone->update(['image_path' => $filename]);
        }
        $phone->update();
        return redirect()->route('phones.index')->with('success','Entry updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {

        Phone::destroy($id);
        return redirect()->route('phones.index')->with('success','Phone deleted successfully');
    }

    public function __construct()
    {
        $this->middleware('auth');
    }


}

<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UpdateProfileRequest;
use Image;

class ProfileController extends Controller
{
    public function index()
    {
        return view('users/profile')->with('user', Auth::user());
    }

    public function update(UpdateProfileRequest $request, User $user, $id): \Illuminate\Http\RedirectResponse
    {
        //Cap nhat thong tin nguoi dung
        $user = User::find($id);

        $user->name = $request->input('name');
        $user->username = $request->input('username');
        $user->email = $request->input('email');

        if($request->hasFile('avatar')) {
            $avatar = request()->file('avatar');
            $extension = $avatar->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $avatar->move(public_path('/uploads/avatars/'), $filename);
            $user->update(['avatar' => $filename]);
        }
        $user->update();
        return redirect()->route('phones')->with('success', 'User updated successfully');

    }

    public function __construct()
    {
        $this->middleware('auth');
    }


}


<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Category;

class Phone extends Model
{
    use HasFactory;
    protected $table = 'phones';
    protected $fillable = ['name','category_id','quantity','price','image_path'];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
}
